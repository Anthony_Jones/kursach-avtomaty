package com.cwteam.main;

import java.util.EmptyStackException;
import java.util.Stack;

public class Translator {
    public static int prioity(char operation) throws ParseException {
        switch (operation) {
            case '*':
            case '/':
                return 3;
            case '-':
            case '+':
                return 2;
            case '(':
            case ')':
                return 1;
        }
        throw new ParseException("What is the operator?");
    }
    public static boolean isOperator(char operator) {
        return operator == '+' || operator == '-' || operator == '*' || operator == '/';
    }
    public static String translate(String infixString) throws ParseException {
        Stack<Character> stack = new Stack<>();
        char[] string = infixString.toCharArray();
        StringBuilder result = new StringBuilder();
        for (char i :
                string) {
            if(Character.isDigit(i)) {
                result.append(i);
            } else if(isOperator(i)) {
                if(stack.isEmpty()) {
                    stack.push(i);
                } else if(prioity(stack.peek()) < prioity(i)) {
                    stack.push(i);
                } else {
                    try {
                        do {
                            result.append(stack.pop());
                        }
                        while (prioity(stack.peek()) >= prioity(i));
                    } catch (EmptyStackException e) {
                        if(stack.isEmpty()) {
                            stack.push(i);
                        }
                    }
                    if(prioity(stack.peek()) < prioity(i)) {
                        stack.push(i);
                    }
                }
            } else if(i == '(') {
                stack.push(i);
            } else if(i == ')') {
                while (stack.peek() != '(') {
                    result.append(stack.pop());
                }
                stack.pop();
            }
        }
        while (!stack.isEmpty()) {
            result.append(stack.pop());
        }
        return result.toString();
    }
}
