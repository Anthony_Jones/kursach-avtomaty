package com.cwteam.main;

public class ParseException extends Exception {
    public ParseException(String message) {
        super(message);
    }
}
