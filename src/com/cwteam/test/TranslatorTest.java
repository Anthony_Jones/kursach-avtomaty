package com.cwteam.test;

import com.cwteam.main.ParseException;
import com.cwteam.main.Translator;


import static org.junit.jupiter.api.Assertions.*;

class TranslatorTest {

    @org.junit.jupiter.api.Test
    void translate() {
        try {
            String result = Translator.translate("1/3*2-(4-2)-3-4*1");
            assertEquals("13/2*42--3-41*-", result);
            result = Translator.translate("1+1");
            assertEquals("11+", result);
            result = Translator.translate("(1+1)");
            assertEquals("11+", result);
            result = Translator.translate("1/1");
            assertEquals("11/", result);
            result = Translator.translate("1/1+2");
            assertEquals("11/2+", result);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}